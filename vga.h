#pragma once

#include <stddef.h>
#include <stdint.h>

// Enums in the C stdlib are lowercase, so we do that too
// In general, it is best to follow the style conventions in the C standard library
typedef enum {
    vga_color_black = 0,
    vga_color_blue = 1,
    vga_color_green = 2,
    vga_color_cyan = 3,
    vga_color_red = 4,
    vga_color_magenta = 5,
    vga_color_brown = 6,
    vga_color_light_grey = 7,
    vga_color_dark_grey = 8,
    vga_color_light_blue = 9,
    vga_color_light_green = 10,
    vga_color_light_cyan = 11,
    vga_color_light_red = 12,
    vga_color_light_magenta = 13,
    vga_color_light_brown = 14,
    vga_color_white = 15,
} vga_color;

// Best to follow convention, which says macros are upper case
// We could also put these in an enum, but I don't think that is a good way to go
#define VGA_WIDTH 80
#define VGA_HEIGHT 25

typedef struct {
    // aligned on 2-byte boundary for perf
    uint16_t text[VGA_HEIGHT][VGA_WIDTH];
    // can be aligned anywhere
    uint8_t color;
    // Current row and column
    size_t row;
    size_t column;
} vga_terminal;

void vga_init(vga_terminal* const term, const vga_color fg, const vga_color bg);

void vga_set_color(vga_terminal* const term, const vga_color fg, const vga_color bg);

void vga_clear(vga_terminal* const term);

void vga_display(const vga_terminal* const term);

void vga_scroll_down(vga_terminal* const term);

void vga_putc(vga_terminal* const term, const char ch);

void vga_puts(vga_terminal* const term, const char* const str);
