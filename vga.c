#include "string.h"
#include "system.h"
#include "vga.h"

// C doesn't have namespaces, so we prepend vga to the start of every identifier
// Well actually, if the function is static then you don't need to prepend
// static functions don't need a prototype (hooray!)

static uint8_t vga_entry_color(const vga_color fg, const vga_color bg) {
    // When doing shifts, each operand is promoted first to an int or unsigned int
    // fg and bg are both ints under the hood, so this is ok, if odd
    // Watch out for left shifts though (sign bit)
    return (uint8_t)(fg | bg << 4);
}

// The first byte specifies the color,
// the second byte specifies the actual character
// Need the ch to be unsigned. If it is signed, integer promotion will sign extend it,
// which then might mess up the |
static uint16_t vga_entry(const uint8_t ch, const uint8_t color) {
    // color is promoted to an int before the shift
    // (and ints are always at least 16 bits wide), so this works a-ok
    return (uint16_t)(ch | color << 8);
}

// maybe we could use vga_create with LTO (or just use a cross-compiler, you know, that too)
void vga_init(vga_terminal* const term, const vga_color fg, const vga_color bg) {
    vga_set_color(term, fg, bg);
    vga_clear(term);
}

void vga_set_color(vga_terminal* const term, const vga_color fg, const vga_color bg) {
    term->color = vga_entry_color(fg, bg);
}

void vga_clear(vga_terminal* const term) {

    // Go back to the beginning
    term->row = 0;
    term->column = 0;

    // Print height * width spaces
    for (size_t i = 0; i < VGA_HEIGHT * VGA_WIDTH; ++i) {
        vga_putc(term, ' ');
    }
}

// Write the terminal to the IO buffer
void vga_display(const vga_terminal* const term) {

    // Needs to be volatile since this is memory mapped IO
    volatile uint16_t* const buffer = (void*)0xB8000;

    for (size_t row = 0; row < VGA_HEIGHT; ++row) {
        for (size_t col = 0; col < VGA_WIDTH; ++col) {
            const size_t index = VGA_WIDTH * row + col;
            buffer[index] = term->text[row][col];
        }
    }
}

void vga_scroll_down(vga_terminal* const term) {
    // we want to move each row up by one, and make the last one blank
    // the rows are stored in row-major order, so we have a bunch
    // of rows. We just want to shift the whole thing by one row of entries
    // over
    // Need to use memmove for this, because they will overlap.

    const uint16_t* const dest = &term->text[0][0];
    const uint16_t* const src = &term->text[1][0];
    const size_t count = VGA_WIDTH * (VGA_HEIGHT - 1);
    kmemmove(dest, src, count);

    // Now set the last row
    const uint16_t entry = vga_entry(' ', term->color);
    kmemset(&term->text[VGA_HEIGHT - 1][0], entry, VGA_WIDTH);
}

void vga_putc(vga_terminal* const term, const char ch) {

    if (ch == '\n') {

        term->column = 0;

        if (term->row == VGA_HEIGHT - 1) {
            vga_scroll_down(term);
        } else {
            term->row += 1;
        }
    } else {

        term->text[term->row][term->column] = vga_entry((uint8_t)ch, term->color);

        // Move over to the next character
        term->column += 1;

        if (term->column == VGA_WIDTH) {
            term->column = 0;
            // We've reached the end of a row,
            // so go down one
            if (term->row == VGA_HEIGHT - 1) {
                vga_scroll_down(term);
            } else {
                term->row += 1;
            }
        }
    }
}

void vga_puts(vga_terminal* const term, const char* const str) {
    const size_t len = strlen(str);
    for (size_t i = 0; i < len; ++i) {
        vga_putc(term, str[i]);
    }
}
