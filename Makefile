CC = clang
CFLAGS = -std=c11 -Weverything -ffreestanding -O2 -target i386-pc-none-elf -march=i386
# -fno-builtin disables intrinsics. But, you shouldn't be developing code based on
#  whether it generates intrinsics or not. The thing to do is use a cross-compiler,
#  which takes care of all of this for you. I think

LD = ld.gold
# doesn't know where to find compiler-rt, which is the equivalent of libgcc for clang
LDFLAGS = -nostdlib -O2 --oformat=elf32-i386

AS = nasm
ASFLAGS = -f elf32

# Clang doesn't support linker scripts. Use plain old gold for now
os.bin: linker.ld boot.o kernel.o vga.o string.o system.o
	$(LD) $(LDFLAGS) -T linker.ld -o os.bin boot.o kernel.o vga.o string.o system.o

boot.o: boot.nasm
	$(AS) $(ASFLAGS) boot.nasm -o boot.o

vga.o: vga.c vga.h
	$(CC) $(CFLAGS) -c vga.c

string.o: string.c string.h
	$(CC) $(CFLAGS) -c string.c

system.o: system.c system.h
	$(CC) $(CFLAGS) -c system.c

kernel.o: kernel.c vga.h
	$(CC) $(CFLAGS) -c kernel.c

.PHONY: clean

clean:
	-rm -f *.o os.bin
