#include "vga.h"

void kmain(void) {

    const char* const str = "Hello World!";

    vga_terminal term;
    vga_init(&term, vga_color_light_grey, vga_color_black);

    vga_puts(&term, str);

    vga_display(&term);
}
