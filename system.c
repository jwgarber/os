#include "system.h"

// I could implement this to conform to the C standard, but I won't. Some functions
// in there suck and are unsafe. Since I need to reimplement the entire thing myself
// I think I am totally justified in making changes and improvements where I feel
// justified. Many of these functions will be based on a Rust interface. So there.

// memmove takes care of overlapping memory regions
// In general, you should convert pointers to a uintptr_t, which gives the memory
// address, which you can then compare
//
// Only use the array syntax for static arrays. Dynamic arrays aren't even a thing
// in C, they're just constructs of malloc.

// Hack-a-doodle-do for templates
#define KMEMSET_DEF(type)                    \
    void kmemset_##type(type* const dest,    \
        const type val,                      \
        const size_t count) {                \
                                             \
        for (size_t i = 0; i < count; ++i) { \
            dest[i] = val;                   \
        }                                    \
    }

KMEMSET_DEF(char)
KMEMSET_DEF(signed_char)
KMEMSET_DEF(unsigned_char)
KMEMSET_DEF(short)
KMEMSET_DEF(unsigned_short)
KMEMSET_DEF(int)
KMEMSET_DEF(unsigned)
KMEMSET_DEF(long)
KMEMSET_DEF(unsigned_long)
KMEMSET_DEF(long_long)
KMEMSET_DEF(unsigned_long_long)

// Assume the arrays don't overlap, hence the restrict
// The restrict applies to the point,r and the second const to the variable holding the pointer
// More intuitive that way
#define KMEMCOPY_DEF(type)                          \
    void kmemcopy_##type(type* restrict const dest, \
        const type* restrict const src,             \
        const size_t count) {                       \
                                                    \
        for (size_t i = 0; i < count; ++i) {        \
            dest[i] = src[i];                       \
        }                                           \
    }

KMEMCOPY_DEF(char)
KMEMCOPY_DEF(signed_char)
KMEMCOPY_DEF(unsigned_char)
KMEMCOPY_DEF(short)
KMEMCOPY_DEF(unsigned_short)
KMEMCOPY_DEF(int)
KMEMCOPY_DEF(unsigned)
KMEMCOPY_DEF(long)
KMEMCOPY_DEF(unsigned_long)
KMEMCOPY_DEF(long_long)
KMEMCOPY_DEF(unsigned_long_long)

// NOTE: it is UB to compare arbitrary pointers. In general, it is best to convert
// them to uintptr_t, and then compare those
// array syntax is less expressive than pointer syntax. You can't make the variable
// itself const, or restrict
// Comparing arbitrary pointers is UB. So we convert them to integers first
// src and dest could overlap, so we start copying from the end of src
// array subscripting is always better than pointer arithmetic
// start copying from the begining of dest
#define KMEMMOVE_DEF(type)                            \
    void kmemmove_##type(type* const dest,            \
        const type* const src,                        \
        const size_t count) {                         \
                                                      \
        if ((uintptr_t)src < (uintptr_t)dest) {       \
            for (size_t i = count - 1; i != 0; --i) { \
                dest[i] = src[i];                     \
            }                                         \
        } else {                                      \
            for (size_t i = 0; i < count; ++i) {      \
                dest[i] = src[i];                     \
            }                                         \
        }                                             \
    }

KMEMMOVE_DEF(char)
KMEMMOVE_DEF(signed_char)
KMEMMOVE_DEF(unsigned_char)
KMEMMOVE_DEF(short)
KMEMMOVE_DEF(unsigned_short)
KMEMMOVE_DEF(int)
KMEMMOVE_DEF(unsigned)
KMEMMOVE_DEF(long)
KMEMMOVE_DEF(unsigned_long)
KMEMMOVE_DEF(long_long)
KMEMMOVE_DEF(unsigned_long_long)
