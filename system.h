#pragma once

#include <stddef.h>
#include <stdint.h>

// For educational purposes, we are doing everything in C right now. But when
// the time comes, it will be very nice to upgrade to C++.

#define generic _Generic

// Need this for the macros
typedef signed char signed_char;
typedef unsigned char unsigned_char;
typedef unsigned short unsigned_short;
typedef unsigned long unsigned_long;
typedef long long long_long;
typedef unsigned long long unsigned_long_long;

// kmemset
#define KMEMSET_DEC(type)                 \
    void kmemset_##type(type* const dest, \
        const type val,                   \
        const size_t count);

KMEMSET_DEC(char)
KMEMSET_DEC(signed_char)
KMEMSET_DEC(unsigned_char)
KMEMSET_DEC(short)
KMEMSET_DEC(unsigned_short)
KMEMSET_DEC(int)
KMEMSET_DEC(unsigned)
KMEMSET_DEC(long)
KMEMSET_DEC(unsigned_long)
KMEMSET_DEC(long_long)
KMEMSET_DEC(unsigned_long_long)

#define kmemset(dest, val, count) generic((dest),                       \
                                          char*                         \
                                          : kmemset_char,               \
                                          signed char*                  \
                                          : kmemset_signed_char,        \
                                          unsigned char*                \
                                          : kmemset_unsigned_char,      \
                                          short*                        \
                                          : kmemset_short,              \
                                          unsigned short*               \
                                          : kmemset_unsigned_short,     \
                                          int*                          \
                                          : kmemset_int,                \
                                          unsigned*                     \
                                          : kmemset_unsigned,           \
                                          long*                         \
                                          : kmemset_long,               \
                                          unsigned long*                \
                                          : kmemset_unsigned_long,      \
                                          long long*                    \
                                          : kmemset_long_long,          \
                                          unsigned long long*           \
                                          : kmemset_unsigned_long_long, \
                                          const char*                   \
                                          : kmemset_char,               \
                                          const signed char*            \
                                          : kmemset_signed_char,        \
                                          const unsigned char*          \
                                          : kmemset_unsigned_char,      \
                                          const short*                  \
                                          : kmemset_short,              \
                                          const unsigned short*         \
                                          : kmemset_unsigned_short,     \
                                          const int*                    \
                                          : kmemset_int,                \
                                          const unsigned*               \
                                          : kmemset_unsigned,           \
                                          const long*                   \
                                          : kmemset_long,               \
                                          const unsigned long*          \
                                          : kmemset_unsigned_long,      \
                                          const long long*              \
                                          : kmemset_long_long,          \
                                          const unsigned long long*     \
                                          : kmemset_unsigned_long_long)(dest, val, count)

// kmemcopy
#define KMEMCOPY_DEC(type)                          \
    void kmemcopy_##type(type* restrict const dest, \
        const type* restrict const src,             \
        const size_t count);

KMEMCOPY_DEC(char)
KMEMCOPY_DEC(signed_char)
KMEMCOPY_DEC(unsigned_char)
KMEMCOPY_DEC(short)
KMEMCOPY_DEC(unsigned_short)
KMEMCOPY_DEC(int)
KMEMCOPY_DEC(unsigned)
KMEMCOPY_DEC(long)
KMEMCOPY_DEC(unsigned_long)
KMEMCOPY_DEC(long_long)
KMEMCOPY_DEC(unsigned_long_long)

#define kmemcopy(dest, src, count) generic((dest),                        \
                                           char*                          \
                                           : kmemcopy_char,               \
                                           signed char*                   \
                                           : kmemcopy_signed_char,        \
                                           unsigned char*                 \
                                           : kmemcopy_unsigned_char,      \
                                           short*                         \
                                           : kmemcopy_short,              \
                                           unsigned short*                \
                                           : kmemcopy_unsigned_short,     \
                                           int*                           \
                                           : kmemcopy_int,                \
                                           unsigned*                      \
                                           : kmemcopy_unsigned,           \
                                           long*                          \
                                           : kmemcopy_long,               \
                                           unsigned long*                 \
                                           : kmemcopy_unsigned_long,      \
                                           long long*                     \
                                           : kmemcopy_long_long,          \
                                           unsigned long long*            \
                                           : kmemcopy_unsigned_long_long, \
                                           const char*                    \
                                           : kmemcopy_char,               \
                                           const signed char*             \
                                           : kmemcopy_signed_char,        \
                                           const unsigned char*           \
                                           : kmemcopy_unsigned_char,      \
                                           const short*                   \
                                           : kmemcopy_short,              \
                                           const unsigned short*          \
                                           : kmemcopy_unsigned_short,     \
                                           const int*                     \
                                           : kmemcopy_int,                \
                                           const unsigned*                \
                                           : kmemcopy_unsigned,           \
                                           const long*                    \
                                           : kmemcopy_long,               \
                                           const unsigned long*           \
                                           : kmemcopy_unsigned_long,      \
                                           const long long*               \
                                           : kmemcopy_long_long,          \
                                           const unsigned long long*      \
                                           : kmemcopy_unsigned_long_long)(dest, src, count)

// kmemmove
#define KMEMMOVE_DEC(type)                 \
    void kmemmove_##type(type* const dest, \
        const type* const src,             \
        const size_t count);

KMEMMOVE_DEC(char)
KMEMMOVE_DEC(signed_char)
KMEMMOVE_DEC(unsigned_char)
KMEMMOVE_DEC(short)
KMEMMOVE_DEC(unsigned_short)
KMEMMOVE_DEC(int)
KMEMMOVE_DEC(unsigned)
KMEMMOVE_DEC(long)
KMEMMOVE_DEC(unsigned_long)
KMEMMOVE_DEC(long_long)
KMEMMOVE_DEC(unsigned_long_long)

#define kmemmove(dest, src, count) generic((dest),                        \
                                           char*                          \
                                           : kmemmove_char,               \
                                           signed char*                   \
                                           : kmemmove_signed_char,        \
                                           unsigned char*                 \
                                           : kmemmove_unsigned_char,      \
                                           short*                         \
                                           : kmemmove_short,              \
                                           unsigned short*                \
                                           : kmemmove_unsigned_short,     \
                                           int*                           \
                                           : kmemmove_int,                \
                                           unsigned*                      \
                                           : kmemmove_unsigned,           \
                                           long*                          \
                                           : kmemmove_long,               \
                                           unsigned long*                 \
                                           : kmemmove_unsigned_long,      \
                                           long long*                     \
                                           : kmemmove_long_long,          \
                                           unsigned long long*            \
                                           : kmemmove_unsigned_long_long, \
                                           const char*                    \
                                           : kmemmove_char,               \
                                           const signed char*             \
                                           : kmemmove_signed_char,        \
                                           const unsigned char*           \
                                           : kmemmove_unsigned_char,      \
                                           const short*                   \
                                           : kmemmove_short,              \
                                           const unsigned short*          \
                                           : kmemmove_unsigned_short,     \
                                           const int*                     \
                                           : kmemmove_int,                \
                                           const unsigned*                \
                                           : kmemmove_unsigned,           \
                                           const long*                    \
                                           : kmemmove_long,               \
                                           const unsigned long*           \
                                           : kmemmove_unsigned_long,      \
                                           const long long*               \
                                           : kmemmove_long_long,          \
                                           const unsigned long long*      \
                                           : kmemmove_unsigned_long_long)(dest, src, count)
